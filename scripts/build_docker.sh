#!/bin/bash
set -euxo pipefail
DIR="$( cd "$( dirname "BASH_SOURCE[0]" )" && pwd )"

cd "$DIR"/../.devcontainer
docker build .
