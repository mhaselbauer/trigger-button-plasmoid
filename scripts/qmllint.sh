#!/bin/bash
set -euxo pipefail
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"/..
QML2_IMPORT_PATH=/usr/lib/x86_64-linux-gnu/qt6/qml:/usr/share/plasma/plasmoids/
export QML2_IMPORT_PATH="$QML2_IMPORT_PATH"
# qmllint -I "/usr/lib/x86_64-linux-gnu/qt6/qml/org/kde/plasma/components/" --dry-run package/contents/ui/main.qml
qmllint --dry-run package/contents/ui/main.qml
