import org.kde.plasma.plasmoid
import org.kde.plasma.components as PlasmaComponents

PlasmoidItem {
    id: root

    compactRepresentation: PlasmaComponents.Button {
        text: "compact representation"
    }
    fullRepresentation: PlasmaComponents.Button {
        text: "full representation"
    }
}
